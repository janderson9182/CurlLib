<?php

namespace CurlLib;

abstract class Curl
{
    abstract protected function execute();

    /**
     * @return resource
     */
    abstract protected function curlInit();

    /**
     * @return string
     */
    abstract protected function getUrl();

    /**
     * @return array
     */
    abstract protected function getCurlOptions();

    /**
     * @return array
     */
    abstract protected function getParameters();
}