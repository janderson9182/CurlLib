<?php

namespace CurlLib;

abstract class Get extends Curl
{
    /**
     * @return string
     */
    public function execute()
    {
        $curl = $this->curlInit();
        $options = $this->getCurlOptions();
        if (count($options) > 0) {
            curl_setopt_array($curl, $options);
        }
        return curl_exec($curl);
    }
    /**
     * @return resource
     */
    final protected function curlInit()
    {
        return curl_init($this->buildUrl());
    }
    /**
     * @return string
     */
    private function buildUrl()
    {
        $query_string = '';
        $parameters = $this->getParameters();
        if (!empty($parameters)) {
            $query_string .= strpos($this->getUrl(), '?') > 0 ? '&' : '?' . http_build_query($parameters, '', '&');
        }
        return $this->getUrl() . $query_string;
    }
}